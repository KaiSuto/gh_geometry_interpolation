﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using RBFInterpolation.Core;

namespace RBFInterpolation
{
    public class GetPoints : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the GetPoints class.
        /// </summary>
        public GetPoints()
          : base("Get Points", "Get Points",
              "Get points from a RBF cylindrical interpolation instance.",
              "Interpolate", "GetFromRBF")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("RBF", "RBF", "A RBF cylindrical interpolation instance.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Theta", "Theta", "An angle coordinate of a cylindrical coordinates.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Z", "Z", "A z coordinate of a cylindrical coordinates.", GH_ParamAccess.list);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddPointParameter("Points", "Pts", "Points", GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            RBFCylindricalInterpolation RBFCylindrical = new RBFCylindricalInterpolation();
            List<double> theta = new List<double>();
            List<double> z = new List<double>();

            DA.GetData(0, ref RBFCylindrical);
            DA.GetDataList(1, theta);
            DA.GetDataList(2, z);

            List<Point3d> pts = new List<Point3d>();
            for (int i = 0; i < theta.Count; i++)
            {
                Point3d pt = RBFCylindrical.P(theta[i], z[i]);
                pts.Add(pt);
            }

            DA.SetDataList(0, pts);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //You can add image files to your project resources and access them like this:
                // return Resources.IconForThisComponent;
                return null;
            }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("fc7f1f6d-973b-454c-ad62-7038dadf2174"); }
        }
    }
}