﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using RBFInterpolation.Core;

namespace RBFInterpolation
{
    public class InterpolatedSurface : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the InterpolatedSurface class.
        /// </summary>
        public InterpolatedSurface()
          : base("Interpolated Surface", "Interpolated Surface",
              "Create a interpolated surface from RBF function.",
              "Interpolate", "GetFromRBF")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("RBF", "RBF", "A RBF cylindrical interpolation instance.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Start Angle", "SA", "A start angle coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddNumberParameter("End Angle", "EA", "A end angle coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddNumberParameter("Start Z", "SZ", "A start z coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddNumberParameter("End Z", "EZ", "A end z coordinate of a cylindrical coordinates", GH_ParamAccess.item);
            pManager.AddIntegerParameter("U Count", "UC", "A count of control points at angle coordinate", GH_ParamAccess.item);
            pManager.AddIntegerParameter("V Count", "VC", "A count of control points at z coordinate", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddSurfaceParameter("Interpolated Surface", "IS", "A RBF interpolated surface.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            RBFCylindricalInterpolation RBFCylindrical = new RBFCylindricalInterpolation();
            List<double> theta = new List<double>();
            List<double> z = new List<double>();
            double thetaStart = -Math.PI;
            double thetaEnd = Math.PI;
            double zStart = 0;
            double zEnd = 10;
            int uCount = 10;
            int vCount = 10;

            if (!DA.GetData(0, ref RBFCylindrical)) { return; }
            DA.GetData(1, ref thetaStart);
            DA.GetData(2, ref thetaEnd);
            DA.GetData(3, ref zStart);
            DA.GetData(4, ref zEnd);
            DA.GetData(5, ref uCount);
            DA.GetData(6, ref vCount);

            List<Point3d> pts = new List<Point3d>();
            for(int i = 0; i < uCount; i++)
            {
                for(int j = 0; j < vCount; j++)
                {
                    double alphaU = ((double)i / (double)uCount);
                    double alphaV = ((double)j / (double)vCount);
                    double theta_ij = thetaStart + alphaU * (thetaEnd - thetaStart);
                    double z_ij = zStart + alphaV * (zEnd - zStart);
                    theta.Add(theta_ij);
                    z.Add(z_ij);
                }
            }
            for (int i = 0; i < theta.Count; i++)
            {
                pts.Add(RBFCylindrical.P(theta[i], z[i]));
            }

            NurbsSurface surf = NurbsSurface.CreateFromPoints(pts, uCount, vCount, 3, 3);

            DA.SetData(0, surf);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //You can add image files to your project resources and access them like this:
                // return Resources.IconForThisComponent;
                return null;
            }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("369b4333-afd7-43f0-8d49-300712a1cb40"); }
        }
    }
}