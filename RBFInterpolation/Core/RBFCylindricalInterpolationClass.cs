﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Geometry;

namespace RBFInterpolation.Core
{
    class RBFCylindricalInterpolation
    {
        #region Constructors
        public RBFCylindricalInterpolation()
        {

        }
        public RBFCylindricalInterpolation(List<Point3d> pts, Line axis, Point3d origin, Vector3d dDir, int nLayers, double rBase, double smooth, double thetaScale, double zScale, double dScale)
        {
            this.thetaScale = thetaScale;
            this.zScale = zScale;
            this.dScale = dScale;
            this.SamplePts = pts;
            this.sampleCylindCoords = ToCylindricalCoordinates(pts, axis, origin, dDir);
            this.sampleCylindCoordsScalized = ScalizeCylindricalCoordinates(this.sampleCylindCoords);
            this.Axis = axis;
            this.Origin = origin;
            this.DDir = dDir;
            this.nLayers = nLayers;
            this.rBase = rBase;
            this.smooth = smooth;
            SetRBFFunction();
        }
        #endregion

        #region Properties
        public List<Point3d> SamplePts { get; set; }
        public Line Axis { set; get; }
        public Point3d Origin { get; set; }
        public Vector3d DDir { get; set; }
        #endregion

        #region Private Members
        private double[,] sampleCylindCoords;
        private double[,] sampleCylindCoordsScalized;
        private double[] theta;
        private double[] z;
        private double[] d;
        private double thetaMin;
        private double thetaMax;
        private double zMin;
        private double zMax;
        private double dMin;
        private double dMax;
        private double thetaLength;
        private double zLength;
        private double dLength;
        private double thetaScale;
        private double zScale;
        private double dScale;
        private alglib.rbfmodel model;
        private int nLayers;
        private double rBase;
        private double smooth;
        #endregion

        #region Private Methods
        private double[,] ToCylindricalCoordinates(List<Point3d> pts, Line axis, Point3d origin, Vector3d dDir)
        {
            double[,] cylindCoords = new double[pts.Count, 3];
            this.theta = new double[pts.Count];
            z = new double[pts.Count];
            d = new double[pts.Count];
            dDir.Unitize();
            for (int i = 0; i < pts.Count; i++)
            {
                Point3d ptOnAxis = axis.ClosestPoint(pts[i], false);
                double d = ptOnAxis.DistanceTo(pts[i]);
                double z = ptOnAxis.DistanceTo(origin);
                Vector3d ptDir = pts[i] - ptOnAxis; ptDir.Unitize();
                Vector3d axisDir = axis.UnitTangent;
                double theta = VectorAngle(dDir, ptDir, axisDir);
                cylindCoords[i, 0] = theta;
                cylindCoords[i, 1] = z;
                cylindCoords[i, 2] = d;
                this.theta[i] = theta;
                this.z[i] = z;
                this.d[i] = d;
            }
            this.thetaMin = this.theta.Min();
            this.thetaMax = this.theta.Max();
            this.zMin = this.z.Min();
            this.zMax = this.z.Max();
            this.dMin = this.d.Min();
            this.dMax = this.d.Max();
            this.thetaLength = this.thetaMax - this.thetaMin;
            this.zLength = this.zMax - this.zMin;
            this.dLength = this.dMax - this.dMin;
            return cylindCoords;
        }
        private double[,] ScalizeCylindricalCoordinates(double[,] cldCoord)
        {
            int n = cldCoord.Length / 3;
            double[,] sampleCylindCoordsScalized = new double[n, 3];
            for (int i = 0; i < n; i++)
            {
                sampleCylindCoordsScalized[i, 0] = (thetaScale / thetaLength) * cldCoord[i, 0];
                sampleCylindCoordsScalized[i, 1] = (zScale / zLength) * cldCoord[i, 1];
                sampleCylindCoordsScalized[i, 2] = (dScale / dLength) * cldCoord[i, 2];
            }
            return sampleCylindCoordsScalized;
        }
        private double VectorAngle(Vector3d a, Vector3d b, Vector3d n)
        {
            double sin_beta = n * Vector3d.CrossProduct(a, b);
            double cos_beta = a * b;
            double angle = Math.Atan2(sin_beta, cos_beta);
            return angle;
        }
        public void SetRBFFunction()
        {
            alglib.rbfcreate(2, 1, out this.model);
            alglib.rbfsetpoints(this.model, this.sampleCylindCoordsScalized);
            alglib.rbfreport rep;
            alglib.rbfsetalgohierarchical(this.model, this.rBase, this.nLayers, this.smooth);
            alglib.rbfbuildmodel(this.model, out rep);
        }
        #endregion

        #region Public Methods
        public double D(double theta, double z)
        {
            double thetaScaled = (thetaScale / thetaLength) * theta;
            double zScaled = (zScale / zLength) * z;
            double dScaled = alglib.rbfcalc2(model, thetaScaled, zScaled);
            double d = (dLength / dScale) * dScaled;
            return d;
        }
        public Point3d P(double theta, double z)
        {
            double d = this.D(theta, z);
            Vector3d pDir = 1.0 * this.DDir;
            pDir.Rotate(theta, this.Axis.UnitTangent);
            Point3d ptOnAxis = this.Origin + z * this.Axis.UnitTangent;
            Point3d p = ptOnAxis + d * pDir;
            return p;
        }
        #endregion
    }
}
