﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using RBFInterpolation.Core;

namespace RBFInterpolation
{
    public class ConstructCylindricalInterpolation : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the ConstructCylindricalInterpolation class.
        /// </summary>
        public ConstructCylindricalInterpolation()
          : base("ConstructCylindricalInterpolation", "ConstCylindInterpolate",
              "Construct a cylindrical interpolation instance.",
              "Interpolate", "Constructor")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Sample Pts", "Pts", "Sampling point to interpolate.", GH_ParamAccess.list);
            pManager.AddPointParameter("Origin", "O", "Origin of a cylindrical coordinate.", GH_ParamAccess.item);
            pManager.AddVectorParameter("D Direction", "DDir", "A vector to distance direction.", GH_ParamAccess.item);
            pManager.AddLineParameter("Axis", "Axis", "A axis of a cylindrical coordinate.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Number of Layers", "nLayers", "A number of layers of a RBF network.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Radius", "R", "A radius of basis function.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Smooth Prameter", "Smooth", "A parameter of smoothing of a RBF network", GH_ParamAccess.item);
            pManager.AddNumberParameter("Theta Scale", "TScale", "A scale parameter about theta for radius of basis function", GH_ParamAccess.item);
            pManager.AddNumberParameter("Z Scale", "ZScale", "A scale parameter about z for radius of basis function", GH_ParamAccess.item);
            pManager.AddNumberParameter("D Scale", "DScale", "A scale parameter about d for radius of basis function", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("RBF", "RBF", "A RBF cylindrical interpolation instance.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<Point3d> samplePts = new List<Point3d>();
            Point3d origin = new Point3d(0, 0, 0);
            Vector3d dDir = new Vector3d(1, 0, 0);
            Line axis = new Line();
            int nLayers = 1;
            double rBase = 1;
            double smooth = 0;
            double thetaScale = 1;
            double zScale = 1;
            double dScale = 1;

            DA.GetDataList(0, samplePts);
            DA.GetData(1, ref origin);
            DA.GetData(2, ref dDir);
            DA.GetData(3, ref axis);
            DA.GetData(4, ref nLayers);
            DA.GetData(5, ref rBase);
            DA.GetData(6, ref smooth);
            DA.GetData(7, ref thetaScale);
            DA.GetData(8, ref zScale);
            DA.GetData(9, ref dScale);

            Point3d ptOnAxis = axis.ClosestPoint(origin, false);

            if (ptOnAxis.DistanceTo(origin) > 1e-2) { return; }
            else
            {
                RBFCylindricalInterpolation RBFCylindrical
                = new RBFCylindricalInterpolation(samplePts, axis, origin, dDir, nLayers, rBase, smooth, thetaScale, zScale, dScale);

                DA.SetData(0, RBFCylindrical);
            }
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                //You can add image files to your project resources and access them like this:
                // return Resources.IconForThisComponent;
                return null;
            }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("6618f5be-8a62-46c0-9701-f4e5e133e49b"); }
        }
    }
}